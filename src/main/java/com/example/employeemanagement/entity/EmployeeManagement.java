package com.example.employeemanagement.entity;

import com.example.employeemanagement.enums.Department;
import com.example.employeemanagement.enums.EmployeeType;
import com.example.employeemanagement.enums.Job;
import com.example.employeemanagement.enums.Position;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class EmployeeManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String employeeId;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Department department;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Position position;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Job job;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private EmployeeType employeeType;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(nullable = false, length = 40)
    private String address;

    @Column(nullable = false)
    private Integer inCome;

    @Column(nullable = false)
    private LocalDate joinDate;

    private LocalDate endDate;
}
