package com.example.employeemanagement.repository;

import com.example.employeemanagement.entity.EmployeeManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeManagementRepository extends JpaRepository<EmployeeManagement, Long> {
}
