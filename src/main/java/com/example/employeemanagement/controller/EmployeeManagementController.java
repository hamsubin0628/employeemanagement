package com.example.employeemanagement.controller;

import com.example.employeemanagement.model.EmployeeManagementItem;
import com.example.employeemanagement.model.EmployeeManagementRequest;
import com.example.employeemanagement.model.EmployeeManagementResponse;
import com.example.employeemanagement.service.EmployeeManagementService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeManagementController {
    private final EmployeeManagementService employeeManagementService;

    @PostMapping("/management")
    public String setEmployeeManagement(@RequestBody EmployeeManagementRequest request){
        employeeManagementService.setEmployeeManagement(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<EmployeeManagementItem> getEmployeeManagementItems(){
        return employeeManagementService.getEmployeeManagements();
    }

    @GetMapping("/detail/{id}")
    public EmployeeManagementResponse getEmployeeManagement(@PathVariable long id) {
        return employeeManagementService.getEmployeeManagement(id);
    }
}
