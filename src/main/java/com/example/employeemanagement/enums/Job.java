package com.example.employeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Job {
    CEO("대표"),
    DEPARTMENT("부서장"),
    LEADER("팀장"),
    MEMBER("팀원");

    public final String jopName;
}
