package com.example.employeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmployeeType {
    PERMANENT("정규직",true),
    CONTRACT("계약직",false);

    public final String employeeTypeName;
    public final Boolean bonus;
}
