package com.example.employeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Department {
    SALE("영업팀"),
    ACCOUNTING("경리부"),
    TECHNICAL_SUPPORT("기술지원팀"),
    SALES("영업팀"),
    RD("개발팀");

    public final String departmentName;
}

