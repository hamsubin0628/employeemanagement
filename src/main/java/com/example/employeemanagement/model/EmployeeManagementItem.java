package com.example.employeemanagement.model;

import com.example.employeemanagement.enums.Department;
import com.example.employeemanagement.enums.EmployeeType;
import com.example.employeemanagement.enums.Position;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeManagementItem {
    private Long id;
    private String department;
    private String name;
    private String position;
    private String employeeType;
    private LocalDate birthDate;
    private String phoneNumber;
    private String address;
    private LocalDate joinDate;
    private LocalDate endDate;
}
