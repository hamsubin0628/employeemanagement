package com.example.employeemanagement.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeManagementResponse {
    private Long id;
    private String departmentEnum;
    private String name;
    private String positionEnum;
    private String employeeTypeEnum;
    private String employeeTypeBonus;
    private LocalDate birthDate;
    private String phoneNumber;
    private String address;
    private LocalDate joinDate;
    private LocalDate endDate;
}
