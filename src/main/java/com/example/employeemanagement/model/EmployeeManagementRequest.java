package com.example.employeemanagement.model;

import com.example.employeemanagement.enums.Department;
import com.example.employeemanagement.enums.EmployeeType;
import com.example.employeemanagement.enums.Job;
import com.example.employeemanagement.enums.Position;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeManagementRequest {
    private String employeeId;

    @Enumerated(value = EnumType.STRING)
    private Department department;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private Position position;

    @Enumerated(value = EnumType.STRING)
    private Job job;

    @Enumerated(value = EnumType.STRING)
    private EmployeeType employeeType;

    private LocalDate birthDate;

    private String phoneNumber;

    private String address;

    @Column(nullable = false)
    private Integer inCome;

    private LocalDate joinDate;

    private LocalDate endDate;
}
