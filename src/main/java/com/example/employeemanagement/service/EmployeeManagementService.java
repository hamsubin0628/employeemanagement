package com.example.employeemanagement.service;

import com.example.employeemanagement.entity.EmployeeManagement;
import com.example.employeemanagement.model.EmployeeManagementItem;
import com.example.employeemanagement.model.EmployeeManagementRequest;
import com.example.employeemanagement.model.EmployeeManagementResponse;
import com.example.employeemanagement.repository.EmployeeManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeManagementService {
    private final EmployeeManagementRepository employeeManagementRepository;

    public void setEmployeeManagement(EmployeeManagementRequest employeeManagementRequest){
        EmployeeManagement addData = new EmployeeManagement();
        addData.setEmployeeId(employeeManagementRequest.getEmployeeId());
        addData.setName(employeeManagementRequest.getName());
        addData.setDepartment(employeeManagementRequest.getDepartment());
        addData.setName(employeeManagementRequest.getName());
        addData.setPosition(employeeManagementRequest.getPosition());
        addData.setJob(employeeManagementRequest.getJob());
        addData.setEmployeeType(employeeManagementRequest.getEmployeeType());
        addData.setBirthDate(employeeManagementRequest.getBirthDate());
        addData.setPhoneNumber(employeeManagementRequest.getPhoneNumber());
        addData.setAddress(employeeManagementRequest.getAddress());
        addData.setInCome(employeeManagementRequest.getInCome());
        addData.setJoinDate(employeeManagementRequest.getJoinDate());
        addData.setEndDate(employeeManagementRequest.getEndDate());

        employeeManagementRepository.save(addData);
    }

    public List<EmployeeManagementItem> getEmployeeManagements(){
        List<EmployeeManagement> originList = employeeManagementRepository.findAll();

        List<EmployeeManagementItem> result = new LinkedList<>();

        for (EmployeeManagement employeeManagement : originList) {
            EmployeeManagementItem addItem = new EmployeeManagementItem();
            addItem.setId(employeeManagement.getId());
            addItem.setDepartment(employeeManagement.getDepartment().getDepartmentName());
            addItem.setName(employeeManagement.getName());
            addItem.setPosition(employeeManagement.getPosition().getPositionName());
            addItem.setEmployeeType(employeeManagement.getEmployeeType().getEmployeeTypeName());
            addItem.setBirthDate(employeeManagement.getBirthDate());
            addItem.setPhoneNumber(employeeManagement.getPhoneNumber());
            addItem.setAddress(employeeManagement.getAddress());
            addItem.setJoinDate(employeeManagement.getJoinDate());
            addItem.setEndDate(employeeManagement.getEndDate());

            result.add(addItem);
        }
        return result;
    }

    public EmployeeManagementResponse getEmployeeManagement(long Id){
        EmployeeManagement originData = employeeManagementRepository.findById(Id).orElseThrow();

        EmployeeManagementResponse response = new EmployeeManagementResponse();
        response.setId(originData.getId());
        response.setDepartmentEnum(originData.getDepartment().getDepartmentName());
        response.setName(originData.getName());
        response.setPositionEnum(originData.getPosition().getPositionName());
        response.setEmployeeTypeEnum(originData.getEmployeeType().getEmployeeTypeName());
        response.setEmployeeTypeBonus(originData.getEmployeeType().getBonus() ? "지급":"지급안됨");
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setJoinDate(originData.getJoinDate());
        response.setEndDate(originData.getEndDate());

        return  response;
    }
}
